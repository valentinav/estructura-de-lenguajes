def existenpadres(variable):
    if variable == '':
        variable = 'No registra'
    return variable

def validarid(lista, id):
    for i in lista:
        if i[0]==id:
            return -1

def pedirdatos():
    
    error=int(input('digite el numero de estudiantes a ingresar: '))
    while error <1:
        print('el numero de estudiantes debe ser mayor a 0')
        error=int(input('digite el numero de estudiantes a ingresar: '))
    n = error

    listaestudiantes=[]
    for i in range(n):
        estudiante = []
        error=int(input('ingrese la identificación del estudiante: '))

        while error <=100:
            print('La identificación del estudiante debe ser un numero mayor a 100')
            error=int(input('digite la identificación del estudiante: '))

        while validarid(listaestudiantes,error)==-1 or error <=100:
            print('la identificación ya existe')
            error = int(input('digite otra identificación: '))
        
        ident = error
        estudiante.append(ident)
        nombre = input('ingrese el nombre del estudiante: ')
        estudiante.append(nombre)
        variable = input('ingrese el nombre del padre: ')
        padre = existenpadres(variable)
        estudiante.append(padre)
        variable = input('ingrese el nombre de la madre: ')
        madre = existenpadres(variable)
        estudiante.append(madre)
        
        error = float(input('ingrese la estatura del estudiante: '))
        while error <1:
            print('La estatura debe ser mayor a 0')
            error=int(input('ingrese la estatura del estudiante: '))
        peso = error
        estudiante.append(peso)

        error = float(input('ingrese el peso del estudiante: '))
        while error <1:
            print('El peso debe ser mayor a 0')
            error=int(input('ingrese el peso del estudiante: '))
        estatura = error/100
        estudiante.append(estatura)

        error = int(input('ingrese la edad del estudiante: '))
        while error <=2:
            print('La edad del estudiante debe ser mayor a 2 años')
            error=int(input('ingrese la edad del estudiante: '))
        edad=error
        estudiante.append(edad)
        listaestudiantes.append(estudiante)
    return listaestudiantes
    
    
def mostrarestudiantes(lista):
    print('')
    print('ESTUDIANTES ')
    for a in lista:
        print('')
        print('Nombre del estudiante: ',a[1])
        print('Padre: ',a[2])
        print('Madre: ',a[3])
        print('Peso: ',a[4])
        print('Estatura: ',a[5])
        print('edad: ',a[6])

def IMC(lista, pos):
    peso = lista[pos][4]
    altura = lista[pos][5]
    imc = peso/(altura**2)
    return imc
    
def estudiantessobrepeso(lista):
    print('')
    print('ESTUDIANTES CON POSIBLES PROBLEMAS DE SOBREPESO')
    contador = 0
    for elemento in lista:
        imc = IMC(lista, contador)
        if(imc>17 or imc<14):
            print('')
            print('Identificación del estudiante:', elemento[0],' Nombre:', elemento[1],' Edad:', elemento[6],' IMC:', imc)
        contador = contador + 1

Lista = pedirdatos()
mostrarestudiantes(Lista)
estudiantessobrepeso(Lista)
