import pymysql
from tkinter import *
import tkinter

cnx = pymysql.connect(host='localhost', port=3306, user='root',passwd='', db='eventos')
cursor = cnx.cursor()

def mostrarDatos():
	txt = Text(ventana, width=80,height=15)
	txt.insert(INSERT,"idEvento\t\teveNombre\n")
	txt.place(x=20, y=300)
	cursor.execute("select * from evento")
	for linea in cursor:
		txt.insert(INSERT,str(linea[0])+"\t\t"+linea[1]+"\t\t\t")
		txt.insert(INSERT,"\n")
	txt.config(state=DISABLED)

def mostrar():
	txt = Text(ventana, width=80,height=15)
	txt.insert(INSERT,"idEvento\t\teveNombre\t\tprecio_Vol_general\t\t\tprecio_Vol_vip\n")
	txt.place(x=20, y=300)
	cursor.execute("select * from evento order by eveNombre asc")
	for linea in cursor:
		txt.insert(INSERT,str(linea[0])+"\t\t"+linea[1]+"\t\t\t"+linea[2]+"\t\t\t"+linea[3])
		txt.insert(INSERT,"\n")
	txt.config(state=DISABLED)

def buscar():
	nombre = nombreEvento.get()
	query = ("select * from evento where eveNombre ='"+str(nombre)+"'")
	cursor.execute(query)
	for linea in cursor:
		tkMessageBox.showinfo("Encontrado","eveNombre: "+str(linea[0]))
		break
	else:
		tkMessageBox.showinfo("Error","No se encontro el evento.")


ventana = Tk()
nombreEvento = StringVar()
colorFondo = "#005"
colorLetra = "#FFF"
ventana.geometry("700x500")
ventana.title("Gestion Eventos")
ventana.configure(background=colorFondo)
lblTitulo = Label(ventana,text="GESTIONAR EVENTOS",background=colorFondo,fg=colorLetra,font=("Arial",20)).place(x=190,y=20)
lblBuscar = Label(ventana,text="Buscar evento",background=colorFondo,fg=colorLetra).place(x=480,y=100)
txtRemove = Entry(ventana,textvariable=nombreEvento).place(x=550,y=100)
btnMostrar = Button(ventana,text="MOSTRAR",command=mostrar).place(x=350,y=240)
btnBuscar = Button(ventana,text="BUSCAR",command=buscar).place(x=580,y=130)
mostrarDatos()
mainloop()
