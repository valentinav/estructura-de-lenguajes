#Funcion que borra el numero
def elimNumero(num,lista):
    cont = lista.count(num)
    aux=0
    while aux != cont:
        lista.remove(num)
        aux=aux+1
#Funcion que ordena las listas            
def ordenarLista (lista):
    listaPos = lista[:]
    listaPos.sort()
    print("Lista Positiva menor a mayor")
    for i in range(len(listaPos)):
        if listaPos[i]>0:
            print(listaPos[i])
    listaNeg = lista[:]
    listaNeg.sort(reverse=True)
    print("Lista Negativa mayor a menor")
    for i in range(len(listaNeg)):
        if listaNeg[i]<0:
            print(listaNeg[i])

num=1
cont=0
lista = []
while num !=0:
    num = int(input("Digite el numero: "))
    lista = lista + [num]
    cont = cont+1
    if num==0:
        lista.remove(0)
op = 1

#Menu
while (op) != 0:
    print("Eliga una opcion")
    print("1. Eliminar numero")
    print("2. Mostrar lista ordenada ")
    print("3. Salir")
    op = int(input("Digite la opcion: "))

    if(op==1):
        print("      Eliminacion de Numero       ")
        n = int(input("Digite el numero: "))
        if n in lista:
            print("Numero Encontrado")
            elimNumero(n,lista)
        else:
            print("El numero NO esta en la lista")

    if(op==2):
        print ("     Lista Ordenada     ")
        ordenarLista(lista)
        
    if(op==3):
        msj = input("Salida Exitosa: ")
        exit()
