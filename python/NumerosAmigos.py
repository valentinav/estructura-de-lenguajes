def sumaDeDivisores(num):
    suma=0
    for i in range(1,num):
        if num % i == 0:
            suma+= i
    return suma

def amigos():
    num1 = int(input('primer numero: '))
    num2 = int(input('segundo numero: '))

    if (sumaDeDivisores(num1) == num2 and sumaDeDivisores(num2) == num1):
        print("los numeros %d y %d son AMIGOS" %(num1,num2))
    else:
        print('No son amigos')

amigos()
