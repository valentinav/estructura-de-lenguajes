#include <stdio.h>
#include <stdlib.h>
#define N 4
#define M 4

void suma (int a, int b, int* resultado);

int main(int argc, char* argv[]){
	
	int matriz[N][M];
	int i;
	int j;
	int par = 0;
	int impar = 0;
	
	printf("ingrese los numeros con que desea llenar la matriz...\n");
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			scanf("%d", &matriz[i][j]);						
		}
		printf("\n");
	}
	
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			printf("%d", matriz[i][j]);			
			printf(" ");			
		}
		printf("\n");
	}
	printf("\n");
	
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			if(i==j){
				printf("%d", matriz[i][j]);	
			}	
			printf(" ");			
		}
		printf("\n");
	}
	printf("\n");
	
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			if((matriz[i][j]%2)==0){
				suma(par, matriz[i][j], &par);
			}
			else{
				suma(impar, matriz[i][j], &impar);
			}			
		}
	}
	printf("La suma de los numeros pares es: %d \n", par);
	printf("La suma de los numeros impares es: %d \n", impar);
		
	return 0;
}

void suma (int a, int b, int* resultado){
	*resultado = a+b;
}
