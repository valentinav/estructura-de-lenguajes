#include <stdlib.h>
#include <stdio.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct{
	char nombre[20];
	int telefono;
	int promedio;
} Estudiante;

int main(int argc, char *argv[]) {
	
	Estudiante est;
	
	printf("Ingrese el nombre del estudiante: \n");
	scanf("%s", est.nombre);
	printf("Ingrese el telefono del estudiante: \n");
	scanf("%d", &est.telefono);
	printf("Ingrese el promedio del estudiante: \n");
	scanf("%d", &est.promedio);
	
	printf("\nNombre: %s, Telefono: %d, Promedio: %d \n", est.nombre, est.telefono, est.promedio);
	
	return 0;
}
