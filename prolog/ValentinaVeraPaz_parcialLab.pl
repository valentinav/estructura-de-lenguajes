tiempo_mecanico(oscar,[3,4]).
tiempo_mecanico(julian,[4,1,2,4]).
tiempo_mecanico(hugo,[3,4,3]).
tiempo_mecanico(diego,[2,3,2,1,1]).

turnosasignados(M):- tiempo_mecanico(M,L1),cantidad(L1,Total),write(Total).

cantidad([],0).
cantidad([_|L1],N):- cantidad(L1,M), N is M+1.

masrapido(M1,M2):- tiempo_mecanico(M1,L1),tiempo_mecanico(M2,L2), suma(L1,Tiempo1), suma(L2,Tiempo2),Tiempo1<Tiempo2, write(M1).
masrapido(M1,M2):- tiempo_mecanico(M1,L1),tiempo_mecanico(M2,L2), suma(L1,Tiempo1), suma(L2,Tiempo2),Tiempo1>Tiempo2, write(M2).
masrapido(M1,M2):- tiempo_mecanico(M1,L1),tiempo_mecanico(M2,L2), suma(L1,Tiempo1), suma(L2,Tiempo2),Tiempo1=Tiempo2, write("empate").


suma([],0).
suma([A|B],N):- suma(B,M),N is  M+A.

mejora(M):- tiempo_mecanico(M,L1), primer(L1,R1),ultimo(L1,R2),R2<R1.

primer([Cabeza|_],Cabeza):-!.

ultimo(L1,R):- invertir(L1,L2),primer(L2,R).

invertir([X],[X]).
invertir([Cabeza|Cola],R):- invertir(Cola,L1),unir(L1,[Cabeza],R).

unir([],L2,L2).
unir([Cabeza|Cola],L2,[Cabeza|R]):- unir(Cola,L2,R).

mascarros(N):-tiempo_mecanico(X,L),cantidad(L,R), R>N, write(X),nl,fail.

