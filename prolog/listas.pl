
%imprimir una lista.
imprimir(L):- write(L).

%imprimir el primer elemento de una lista.
imprimir_cabeza([X|_]):- write(X).

% [a|L] lista con el elemento "a" en la cabeza y el resto en la
% variable L(cola).
lista_inicia_a([a|_]).

%buscar un elemento en la lista.
buscar(X, [X|_]).
buscar(X, [_|Cola]) :- buscar(X, Cola).

%Calcular el numero de elemento de una lista.
nuel([],0).
nuel([_|Y],N):- nuel(Y,M), N is M+1.


%comparar si dos lista son iguales.
comparar(X,Y):- X=Y.
comparar([_|L1],L2):- comparar(L1,L2). %

%Se recibe un elemento y una lista. Se determina si elelemento
% resibido es el �ltimo elemento de la lista
buscar_num([],0).
buscar_num(X,[_|Cola]):-[X]=Cola; buscar_num(X,Cola).

%concatenar dos listas.
concatenar([],L,L).
concatenar([X|L1],L2,[X|L3]):-concatenar(L1,L2,L3).

%eliminar un elemento de una lista
elimina(X,[X|T],T).
elimina(X,[H|T],[H|T1]):- elimina(X,T,T1).

% Si queremos calcular la inversa de una lista.
% La inversa de una lista vacia es una lista vacia.
% La inversa de H|T es la inversa de T concatenada con H.

inversa([],[]).
inversa([H|T],L):-  inversa(T,R),  concatenar(R,[H],L).

% Utilizando un parametro acumulador.
inver(L1,L2):-inver(L1,L2,[]).
inver([],L,L).
inver([H|T],L,S):-inver(T,L,[H|S]).




















