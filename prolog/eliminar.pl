elimina_x([],X,[]):-!.
elimina_x([X],X,[]):-!.
elimina_x([X|M],X,S):-elimina_x(M,X,S),!.
elimina_x([R|M],X,S):-lista(R), elimina_x(R,X,T), elimina_x(M,X,P), concatenar([T],P,S).
elimina_x([R|M],X,S):-elimina_x(M,X,T), concatenar([R],T,S).

concatenar([],L,L).
concatenar([X|M],L,[X|Z]):-concatenar(M,L,Z).
lista([]):-!.
lista([X|Y]):-lista(Y).
