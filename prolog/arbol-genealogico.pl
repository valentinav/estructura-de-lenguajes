es_padre(francisco,alicia).
es_padre(francisco,eduardo).
es_padre(victoria,eduardo).
es_padre(victoria,alicia).

es_padre(eduardo,luis).
es_padre(veronica,luis).

es_padre(mario,beatriz).
es_padre(alicia,beatriz).

esposos(francisco,victoria).
esposos(eduardo,veronica).
esposos(mario,alicia).

sonEsposos(X,Y):- esposos(X,Y); esposos(Y,X).
tieneHijos(X):- es_padre(X,_).
nietoDe(X):- es_padre(X,H),es_padre(H,N), write(N).
abuela(X):- es_padre(X,H), es_padre(H,_).
esposoDe(X):-sonEsposos(X,Y),write(Y).
