esposos(francisco,victoria).
esposos(mario,alicia).
esposos(eduardo,veronica).

padre(francisco,alicia).
padre(francisco,eduardo).
padre(mario,beatriz).
padre(eduardo,luis).

padre(victoria,eduardo).
padre(victoria,alicia).
padre(alicia,beatriz).
padre(veronica,luis).

buscarHijos(X):- padre(X,_);madre(X,_).
sonEsposos(X,Y):- esposos(X,Y);esposos(Y,X).
nietoDe(X):-  padre(X,G), padre(G,S), write(S).
esAbuela(X):-padre(X,G),padre(G,_).
quienEsHermanoDe(X,Y):- padre(F,X),padre(F,Y),esposos(F,_),Y\=X.
