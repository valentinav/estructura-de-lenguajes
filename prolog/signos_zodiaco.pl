signo(20,1,18,2,acuario).
signo(19,2,20,3,piscis).
signo(21,3,19,4,aries).
signo(20,4,20,5,tauro).
signo(21,5,20,6,geminis).
signo(21,6,22,7,cancer).
signo(23,7,22,8,leo).
signo(23,8,22,9,virgo).
signo(23,9,22,10,libra).
signo(23,10,21,11,escorpio).
signo(22,11,21,12,sagitario).
signo(22,12,19,1,capricornio).

signo(D,M,S):-signo(X1,Y1,_,_,S), D>=X1,D=<31,M=Y1.
signo(D,M,S):-signo(_,_,X2,Y2,S), D=<X2,D>=1,M=Y2.

/*>==<*/
