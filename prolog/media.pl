suma([X],X).
suma([A|B],N):- suma(B,M),N is  M+A.

total([_],1).
total([_|L1],N):- total(L1,M), N is M+1.

promedio([],0).
promedio([A],A).
promedio(L1,P):- suma(L1,S), total(L1,T), P is S/T.


