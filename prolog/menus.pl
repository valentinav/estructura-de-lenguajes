entrada(ensalada).
entrada(queso).
entrada(patacon).
carne(milanesa).
carne(lomo_viche).
pescado(tilapia_frita).
pescado(trucha).
postre(helado).
postre(fruta).

principal(X) :- carne(X).
principal(X) :- pescado(X).

menu(X,Y,Z):- entrada(X),principal(Y),postre(Z).
menu(X,Y):-entrada(X),principal(Y).
menu(X,Y):-principal(X),postre(Y).
