trabaja(maria,ventas).
trabaja(juan,ventas).
trabaja(roque,ventas).

trabaja(nora,compras).
trabaja(pedro,compras).

trabaja(felipe,administracion).
trabaja(hugo,administracion).
trabaja(ana,administracion).

empleada(maria).
empleada(nora).
empleada(felipe).
empleada(hugo).

aprendiz(juan).
aprendiz(roque).
aprendiz(pedro).
aprendiz(ana).

departamento(X):- trabaja(E,X),write(E),nl,fail.
ordenes(A,B):- empleada(A),trabaja(A,ventas),trabaja(B,ventas).
ordenes(A,B):- empleada(A),trabaja(A,compras),trabaja(B,compras).
ordenes(A,B):- empleada(A),trabaja(A,administracion),trabaja(B,administracion).
