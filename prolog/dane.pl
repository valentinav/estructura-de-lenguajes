desempleo(bogota,[10,10.5,9,12,12]).
desempleo(medellin,[5,7,9,10,9,10]).
desempleo(cali,[15,15,14,10,10,9]).

suma([X],X).
suma([A|B],N):- suma(B,M),N is  M+A.

total([_],1).
total([_|L1],N):- total(L1,M), N is M+1.

promedio([],0).
promedio([A],A).
promedio(X,P):- desempleo(X,L1),suma(L1,S), total(L1,T), P is S/T.


