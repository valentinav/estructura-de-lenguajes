#lang racket
(require rnrs/mutable-pairs-6)

;=====================
; FUNCION QUE CALCULA EL IMC
;=====================
  (define(calcular-imc peso altura)
    (/ peso (* altura altura))
  )
;=====================
; FUNCIÓN DE CREACIÓN
;=====================
(define (crear-paciente identificacion nombre apellido genero peso estatura)
  (list identificacion nombre apellido genero peso estatura (calcular-imc peso estatura)
  )
)
;====================================
; MENU
;====================================
(define (pedir-opcion)
  (newline)
  (display "Elige una opcion" )
  (newline)
  (display "1 -> Introducir un paciente desde el teclado" )
  (newline)
  (display "2 -> Mostrar los pacientes por pantalla" )
  (newline)
  (display "3 -> Buscar paciente" ) 
  (newline)
  (display "4 -> Buscar Alertas" )
  (newline)
  (display "5 -> Modificar Datos" )
  (newline)
  (display "6 -> Eliminar Paciente" )
  (newline)
  (display "0 -> Iniciar Programa Nuevo" )
  (newline)
  (newline)
  (display " --> ")
  ;; lee la opción elegida
  (read)
  )
;;====================================
;;================================================ 
;; LEE UNA CADENA DEL TECLADO Y LA DEVUELVE
;; Parámetro:
;; mensaje: cadena de texto que indica el dato que se solicita
(define (leer-teclado mensaje)
  (display mensaje)
  (display " --> ")
  (read)
)
(define (leer-teclado-cadena mensaje)
  (display mensaje)
  (display " --> ")
  ;; Elimina el carácter de salto de línea #\newline, si existe
  (cond (char=? (peek-char) #\newline) 
      (read-char)
  )
  ;; Lee los caracteres
  ;; hasta que encuentra el carácter de salto de línea #\newline
  (do
      (
       (cadena (make-string 0) (string-append cadena (string caracter)))
       (caracter (read-char) (read-char))
      )
    ;; condición de salida
    ((char=? #\newline caracter)
     ;; devuelve la cadena leída
     cadena
    )
    ;; no hay cuerpo del bucle do
   )
)
;;================================================
;; INTRODUCIR LOS DATOS DE UN PACIENTE DESDE EL TECLADO
(define (leer-paciente-teclado)
  (crear-paciente
   (leer-teclado "Identificacion del paciente : ")
   (leer-teclado "Nombre del paciente : ")
   (leer-teclado "Apellido del paciente : ")
   (leer-teclado "Genero (M masculino - F femenino): ")
   (leer-teclado "Peso (en kilogramos): ")
   (leer-teclado "Estatura (en metros): ")
   )
)
;;=============================================
;; FUNCIÓN QUE PONE LAS COMILLAS INICIALES Y FINALES A UN TEXTO
;; Y LO DEVUELVE COMO CADENA
;; Parámetro:
;; texto: caracteres a los que se les van a poner las comillas
;;
(define (poner-comillas texto)
  (string-append (string #\") texto (string #\"))
)
;;=============================================
;;================================================
;;FUNCION PARA ORDENAR EL IMC
(define (extraer_imc lista_con_imc)
  (list-ref lista_con_imc 6)
)

(define (peso-menor lista)
  (cond
    ((< (extraer_imc lista) 18.5)
     (display "peso menor de lo normal")
    )
   )
)

(define (peso-normal lista)
  (cond
    ((> (extraer_imc lista) 18.5)
     (cond
       ((< (extraer_imc lista) 24.9)
        (display "Peso normal")
       )
     )
    )
   )
)

(define (peso-mayor lista)
  (cond
    ((> (extraer_imc lista) 25)
     (cond
       ((< (extraer_imc lista) 29.9)
        (display "Peso mayor de lo normal")
       )
     )
    )
   )
)

(define (peso-obeso lista)
  (cond
    ((> (extraer_imc lista) 30)
     (display "obeso")
    )
   )
)
;;================================================
;;FUNCION QUE MUESTRA LA LISTA DE PACIENTES
;;================================================
(define (mostrar-pacientes lista_pacientes)
  (do
      (
       (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
       
      )
      ;; Condición de salida del bucle
      ((null? lista_auxiliar)
         (display "No hay pacientes registrados...")
         (newline)
         (newline)
      )
    
    ;; Cuerpo del bucle
        (display "Identificacion: ")
        (display (list-ref (car lista_auxiliar) 0))
        (newline)
        (display "Nombre:  ")
        (display (list-ref (car lista_auxiliar) 1))
        (newline)
        (display "apellido: ")
        (display (list-ref (car lista_auxiliar) 2))
        (newline)
        (display "genero: ")
        (display (list-ref (car lista_auxiliar) 3))
        (newline)
        (display "peso: ")
        (display (list-ref (car lista_auxiliar) 4))
        (newline)
        (display "estatura: ")
        (display (list-ref (car lista_auxiliar) 5))
        (newline)
        (display "imc: ")
        (display (list-ref (car lista_auxiliar) 6))
        (newline)
        (display "Diagnostico: ")
        (peso-menor (car lista_auxiliar))
        (peso-normal (car lista_auxiliar))
        (peso-mayor (car lista_auxiliar))
        (peso-obeso (car lista_auxiliar))
        (newline)
        (newline)
  )
)
;;================================================
;;FUNCION QUE PIDE LA IDENTIFICACION DEL PACIENTE
  (define (pedir-id)
    (leer-teclado "Identificacion del paciente : ")
  )
;;================================================
;;BUSCAR PACIENTE
;;==================================================
 (define (buscar-paciente lista_pacientes )

    (define ip (pedir-id))
    (do
       (
          (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
       )
       ;; Condición de salida del bucle
       ((null? lista_auxiliar)
          (display "No hay pacientes registrados...")
          (newline)
          (newline)
       )
       ;; Cuerpo del bucle 
       (cond
         ((= ip (list-ref (car lista_auxiliar) 0))
           (display "Identificacion: ")
           (display (list-ref (car lista_auxiliar) 0))
           (newline)
           (display "Nombre:  ")
           (display (list-ref (car lista_auxiliar) 1))
           (newline)
           (display "apellido: ")
           (display (list-ref (car lista_auxiliar) 2))
           (newline)
           (display "genero: ")
           (display (list-ref (car lista_auxiliar) 3))
           (newline)
           (display "peso: ")
           (display (list-ref (car lista_auxiliar) 4))
           (newline)
           (display "estatura: ")
           (display (list-ref (car lista_auxiliar) 5))
           (newline)
           (display "imc: ")
           (display (list-ref (car lista_auxiliar) 6))
           (newline)
           (display "Diagnostico: ")
           (peso-menor (car lista_auxiliar))
           (peso-normal (car lista_auxiliar))
           (peso-mayor (car lista_auxiliar))
           (peso-obeso (car lista_auxiliar))
           (newline)
           (newline)
         )
         
       )
    )    
  )


;;================================================
;;BUSCAR ALERTAS
;;================================================
  (define (buscar-alertas lista_pacientes)
    (display "PESO INFERIOR AL NORMAL < 18.5")
    (newline)
    (do
       (
          (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
          ;(id (pedir-id) (pedir-id))
       )
       ;; Condición de salida del bucle
       ((null? lista_auxiliar)
          (newline)
       )
       ;; Cuerpo del bucle 
       (cond
         ((< (extraer_imc (car lista_auxiliar)) 18.5)
           (display "Identificacion: ")
           (display (list-ref (car lista_auxiliar) 0))
           (newline)
           (display "Nombre:  ")
           (display (list-ref (car lista_auxiliar) 1))
           (newline)
           (display "apellido: ")
           (display (list-ref (car lista_auxiliar) 2))
           (newline)
           (display "genero: ")
           (display (list-ref (car lista_auxiliar) 3))
           (newline)
           (display "peso: ")
           (display (list-ref (car lista_auxiliar) 4))
           (newline)
           (display "estatura: ")
           (display (list-ref (car lista_auxiliar) 5))
           (newline)
           (display "imc: ")
           (display (list-ref (car lista_auxiliar) 6))
           (newline)
           (display "Diagnostico: ")
           (peso-menor (car lista_auxiliar))
           (newline)
           (newline)
         )
         
       )
    )
;;==================================================
    (display "PESO SUPERIOR AL NORMAL 25.0 - 29.9")
    (newline)
    (do
       (
          (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
          ;(id (pedir-id) (pedir-id))
       )
       ;; Condición de salida del bucle
       ((null? lista_auxiliar)
          (newline)
       )
       ;; Cuerpo del bucle 
       (cond
         ((> (extraer_imc (car lista_auxiliar)) 25) 
           (cond
             ((<(extraer_imc (car lista_auxiliar)) 29.9)
              (display "Identificacion: ")
              (display (list-ref (car lista_auxiliar) 0))
              (newline)
              (display "Nombre:  ")
              (display (list-ref (car lista_auxiliar) 1))
              (newline)
              (display "apellido: ")
              (display (list-ref (car lista_auxiliar) 2))
              (newline)
              (display "genero: ")
              (display (list-ref (car lista_auxiliar) 3))
              (newline)
              (display "peso: ")
              (display (list-ref (car lista_auxiliar) 4))
              (newline)
              (display "estatura: ")
              (display (list-ref (car lista_auxiliar) 5))
              (newline)
              (display "imc: ")
              (display (list-ref (car lista_auxiliar) 6))
              (newline)
              (display "Diagnostico: ")
              (peso-mayor (car lista_auxiliar))
              (newline)
              (newline)
             )
           )
         )
       )
    )
;;==================================================
    (display "OBESIDAD < 30")
    (newline)
    (do
       (
          (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
          ;(id (pedir-id) (pedir-id))
       )
       ;; Condición de salida del bucle
       ((null? lista_auxiliar)
          (newline)
       )
       ;; Cuerpo del bucle 
       (cond
         ((> (extraer_imc (car lista_auxiliar)) 30)
           (display "Identificacion: ")
           (display (list-ref (car lista_auxiliar) 0))
           (newline)
           (display "Nombre:  ")
           (display (list-ref (car lista_auxiliar) 1))
           (newline)
           (display "apellido: ")
           (display (list-ref (car lista_auxiliar) 2))
           (newline)
           (display "genero: ")
           (display (list-ref (car lista_auxiliar) 3))
           (newline)
           (display "peso: ")
           (display (list-ref (car lista_auxiliar) 4))
           (newline)
           (display "estatura: ")
           (display (list-ref (car lista_auxiliar) 5))
           (newline)
           (display "imc: ")
           (display (list-ref (car lista_auxiliar) 6))
           (newline)
           (display "Diagnostico: ")
           (peso-menor (car lista_auxiliar))
           (peso-normal (car lista_auxiliar))
           (peso-mayor (car lista_auxiliar))
           (newline)
           (newline)
         )
         
       )
    )

  )
;;====================================================
;;ELIMINAR PACIENTE
;;====================================================
(define (eliminar lista_pacientes)
  (define ip (pedir-id))
  (do
       (
          (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
       )
       ;; Condición de salida del bucle
       ((null? lista_auxiliar)
          (newline)
       )
       ;; Cuerpo del bucle 
       (cond
         ((= ip (list-ref (car lista_auxiliar) 0))
          (display "Entro ------")
          (newline)
          (define pi (car lista_auxiliar))
          (newline)
          (newline)
          (set! lista_pacientes (remove (assv ip lista_pacientes) lista_pacientes))
          (newline)
          (programa lista_pacientes)
         )
       )
    )
)
;;====================================================
;;MODIFICAR PACIENTE
;;====================================================
;;CREA UN PACIENTE CON UN NUEVO PESO
(define (paciente_peso_modificado lista)
  (crear-paciente
   (list-ref lista 0)
   (list-ref lista 1)
   (list-ref lista 2)
   (list-ref lista 3)
   (leer-teclado "Peso  Nuevo(en kilogramos): ")
   (list-ref lista 5)
   )
)

;;CREA UN PACIENTE CON UNA NUEVA ESTATURA
(define (paciente_estatura_modificada lista)
  (crear-paciente
   (list-ref lista 0)
   (list-ref lista 1)
   (list-ref lista 2)
   (list-ref lista 3)
   (list-ref lista 4)
   (leer-teclado "Estatura  Nueva(en metros): ")
   )
)

;;MODIFICA/ACTUALIZA LOS DATOS 
(define (modificar-paciente lista_pacientes )
    (display "1--> Modificar Peso")
    (newline)
    (display "2--> Modificar Estatura")
    (newline)
    (define opc (leer-teclado "opcion: "))
    (define ip (pedir-id))

    (cond
        ((= opc 1)
         (do
             (
              (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
              )
           ;; Condición de salida del bucle
            ((null? lista_auxiliar)
             (newline)
            )
           ;; Cuerpo del bucle 
           (cond
             ((= ip (list-ref (car lista_auxiliar) 0))
              (set! lista_pacientes (append lista_pacientes (list (paciente_peso_modificado(car lista_auxiliar)))))
              (set! lista_pacientes (remove (assv ip lista_pacientes) lista_pacientes)) 
              (newline)
              (display "Peso del paciente Modificado correctamente")
              (programa lista_pacientes)
             )
           )
         )
       )
       ((= opc 2)
         (do
             (
              (lista_auxiliar lista_pacientes (cdr lista_auxiliar))
              )
           ;; Condición de salida del bucle
            ((null? lista_auxiliar)
             (newline)
            )
           ;; Cuerpo del bucle 
           (cond
             ((= ip (list-ref (car lista_auxiliar) 0))
              (set! lista_pacientes (append lista_pacientes (list (paciente_estatura_modificada(car lista_auxiliar)))))
              (set! lista_pacientes (remove (assv ip lista_pacientes) lista_pacientes)) 
              (newline)
              (display "Estatura del paciente Modificada correctamente")
              (programa lista_pacientes)
             )
           )
         )
       )
   )

)
;;=============================================================================================================================
;;=============================================================================================================================
;;PROGRAMA PRINCIPAL                          ||
;;=============================================
(define(programa pacientes)

(do
    ;; Variables
    (
     ;; LISTA EN LA QUE SE VAN A ALMACENAR LOS PACIENTE
     ;(pacientes '())
     ;(lista1 '())
     (opcion (pedir-opcion) (pedir-opcion))
    )
  ;; condicion de salida
  ((= opcion 0)
   (display "Iniciar programa nuevo")
   (exit (programa '()))
  )
  
  ;; cuerpo del bucle
  (cond
    ;; INTRODUCIR UN PACIENTE DESDE EL TECLADO
    ((= opcion 1)
     (display "Introduccion de datos de un paciente")
     (newline)
     ;; Uso obligatorio de set!
     (set! pacientes (append pacientes (list (leer-paciente-teclado))))
    )
    ;; MOSTRAR LOS PACIENTES POR LA PANTALLA
    ((= opcion 2)
     (mostrar-pacientes pacientes)
    )
    ;; BUSCAR y MOSTRAR LOS DATOS DE UN PACIENTE POR PANTALLA
    ((= opcion 3)
     (buscar-paciente pacientes)
    )
    ;; MOSTRAR LAS ALERTAS POR PANTALLA
    ((= opcion 4)
     (buscar-alertas pacientes)
    )
    ;; MODIFICAR/ACTUALIZAR DATOS DE UN PACIENTE
    ((= opcion 5)
     (modificar-paciente pacientes)
    )
    ;; ELIMINAR UN PACIENTE 
    ((= opcion 6)
     (eliminar pacientes)
    )
    (else (display "Opcion incorrecta")
      (newline)
    )
   )
   (leer-teclado-cadena "Pulse \"Enter\" para continuar")
   (newline)
 )
)

(programa '())
