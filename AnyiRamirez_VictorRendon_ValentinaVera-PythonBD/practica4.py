##Anyi Ramirez, Victor Rendón, Valentina Vera
import pymysql
import os
import math
from datetime import datetime
from tabulate import tabulate

##VALIDA FECHA
def validateDateEs(date):
    while True:
        try:
            fecha_str = date
            fecha = datetime.strptime(fecha_str, '%Y-%m-%d')
            return True
        except ValueError:
            print("No ha ingresado una fecha correcta...")
            return False
        else:
            break

##OPCIONES MENU##
def menu():
    """
    Función que limpia la pantalla y muestra nuevamente el menu
    """
    os.system('cls')
    print ("Selecciona una opción")
    print ("\t1 - Agregar Datos Al Evento")
    print ("\t2 - Buscar Evento")
    print ("\t3 - Mostrar Eventos")
    print ("\t4 - Comprar Entrada")
    print ("\t5 - salir")

def menuBusqueda():
    """
    Función que limpia la pantalla y muestra nuevamente el menu
    """
    os.system('cls')
    print ("Selecciona una opción")
    print ("\t1 - Buscar Por Codigo")
    print ("\t2 - Buscar Por Nombre")
    print ("\t3 - Buscar Por Fecha")
    print ("\t4 - salir")

##CONEXION A LA BASE DE DATOS##
conn = pymysql.connect(
    host='localhost',
    port=3306,
    user='root',
    passwd='',
    db='gestionevento')

try:
    while True:
        menu()
        opcionMenu = input("Selecciona una opcion: ")
        if opcionMenu=="1":
            print ("")
            print("Has pulsado la opción 1...\n")
            try:
                valCodigo = 1
                while (valCodigo == 1):
                    codEvento = int(input("Agregue Codigo Del Evento: "))
                    with conn.cursor() as cursor:
                        valCodigo = cursor.execute("SELECT * From evento WHERE `codigo` = '"+str(codEvento)+"'")
                        if valCodigo == 1:
                            print("Ya se encuentra registado el Codigo")
                    conn.commit()
                nomEvento = input("Agregue el Nombre Del Evento: ")
                numEntradas = int(input("Agregue El Numero De Entradas A Vender: "))
                dateEvento = input("Agregue la Fecha (AÑO-MES-DIA): ")
                validacion = validateDateEs(dateEvento)
                while validacion == False:
                    dateEvento = input("Agregue la Fecha (AÑO-MES-DIA): ")
                    validacion = validateDateEs(dateEvento)
                with conn.cursor() as cursor:
                    cursor.execute("INSERT INTO evento (`codigo`,`nombre`,`numEntradas`,`fecha`) VALUES ('"+str(codEvento)+"','"+nomEvento.lower()+"','"+str(numEntradas)+"','"+dateEvento+"')")
                conn.commit()
            except ValueError:
                print("Codigo debe ser numerico")
        if opcionMenu=="2":
            while True:
                menuBusqueda()
                opc = input("Seleccione una opcion: ")
                if opc == "1":
                    try:
                        codEvento = int(input("Indique EL Codigo Del Evento: "))
                        with conn.cursor() as cursor:
                            cursor.execute("SELECT `codigo`, `nombre`, `numEntradas`, `fecha` FROM `evento` WHERE codigo = '"+str(codEvento)+"'")
                            rows = [cursor.fetchall()]
                            print("")
                            for eventos in rows:
                                print(tabulate(eventos, headers=['Codigo','Nombre','Entradas','Fecha'], tablefmt='fancy_grid', stralign='left'))
                        conn.commit()
                    except ValueError:
                        print("Codigo debe ser numerico")
                if opc == "2":
                    nomEvento = input("Indique el Nombre Del Evento: ")
                    with conn.cursor() as cursor:
                        cursor.execute("SELECT `codigo`, `nombre`, `numEntradas`, `fecha` FROM `evento` WHERE nombre = '"+nomEvento.lower()+"'")
                        rows = [cursor.fetchall()]
                        print("")
                        for eventos in rows:
                            print(tabulate(eventos, headers=['Codigo','Nombre','Entradas','Fecha'], tablefmt='fancy_grid', stralign='left'))
                    conn.commit()
                if opc == "3":
                    dateEvento = input("Digite la Fecha (AÑO-MES-DIA): ")
                    validacion = validateDateEs(dateEvento)
                    while validacion == False:
                        dateEvento = input("Agregue la Fecha (AÑO-MES-DIA): ")
                        validacion = validateDateEs(dateEvento)
                    with conn.cursor() as cursor:
                        cursor.execute("SELECT `codigo`, `nombre`, `numEntradas`, `fecha` FROM `evento` WHERE fecha = '"+dateEvento+"'")
                        rows = [cursor.fetchall()]
                        print("")
                        for eventos in rows:
                            print(tabulate(eventos, headers=['Codigo','Nombre','Entradas','Fecha'], tablefmt='fancy_grid', stralign='left'))
                    conn.commit()
                if opc == "4":
                    print("Volviendo al menu anterior")
                    break
                else:
                    print ("")
                    input("pulsa una tecla para continuar")
        if opcionMenu == "3":
            with conn.cursor() as cursor:
                cursor.execute("SELECT * FROM `evento`")
                rows = [cursor.fetchall()]
                print("")
                for eventos in rows:
                    print(tabulate(eventos, headers=['Codigo','Nombre','Entradas','Fecha'], tablefmt='fancy_grid', stralign='left'))
                    
            conn.commit()
        if opcionMenu == "4":
            try:
                codEvento = int(input("Agregue Codigo Del Evento: "))
                numEntradas = int(input("Agregue El Numero De Entradas A Vender: "))
                with conn.cursor() as cursor:
                    temporal = cursor.execute("SELECT `numEntradas` FROM `evento` WHERE codigo ='"+str(codEvento)+"' AND fecha > CURDATE() AND numEntradas >= '"+str(numEntradas)+"' ;")
                    if temporal == 1:
                        with conn.cursor() as cursor:
                            temp = cursor.execute("UPDATE evento SET numEntradas = numEntradas - '"+str(numEntradas)+"' WHERE codigo = '"+str(codEvento)+"' AND fecha > CURDATE();")
                        if temp == 1:
                            print("Entradas Vendidas .... ")
                            with conn.cursor() as cursor:
                                temp = cursor.execute("SELECT * FROM `evento` WHERE codigo ='"+str(codEvento)+"';")
                                rows = [cursor.fetchall()]
                                print("")
                                for eventos in rows:
                                    print(tabulate(eventos, headers=['Codigo','Nombre','Entradas','Fecha'], tablefmt='fancy_grid', stralign='left'))
                            conn.commit()
                        conn.commit()
                    if temporal != 1:
                        print("\nCodigo , Fecha(Validad hasta 1 dia antes del evento) o Numero de entradas NO validos\nVerifique la informacion con la opcion 2 ó 3 del menu principal\n")
                conn.commit()
            except ValueError:
                print("Codigo debe ser numerico")
        if opcionMenu == "5":
            print ("Saliendo....")
            break
        else:
            print ("")
            input("Pulsa una tecla para continuar")
except ValueError:
    print("Error Conexion")
finally:
    ##print("conexion no realizada...")
    pass

cursor.close()
conn.close()

